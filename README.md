# FanfareHub

An Intranet for Music Band

**Table of content**

- [Give a try](#give-a-try)
- [Installation](#installation)
- [Deployment](#deployment)
- [Structure](#structure)
- [Development](#development)

## Give a try

On a Debian-based host - running at least Debian Bullseye:

```shell
$ sudo apt install python3 python3-venv git make
$ git clone https://framagit.org/labrigadedestubes/fanfarehub
$ cd fanfarehub/

$ make init
# A configuration will be created interactively; you can uncomment:
#  ENV=development

$ make serve
```

Then visit [http://127.0.0.1:8000/](http://127.0.0.1:8000/) in your web browser.

## Installation
### Requirements

On a Debian-based host, you will need the following packages:
- `make`
- `python3`
- `python3-venv`
- `python3-mysqldb` (in case of a MySQL or MariaDB database)
- `python3-psycopg2` (in case of a PostgreSQL database)
- `git` (recommended for getting the source)

### Initialization and updates

It assumes that you already have the application source code locally - the best
way is by cloning this repository - and that you are in this folder.

1.  Define your local configuration in a file named `config.env`, which can be
    copied from `config.env.example` and edited to suits your needs.

    Depending on your environment, you will have to create your database and the
    user at first.

2.  Run `make init`.

    Note that if there is no `config.env` file, it will be created interactively.

That's it! Your environment is now initialized with the application installed.
To update it, once the source code is checked out, simply run `make update`.

You can also check that your application is well configured by running
`make check`.

## Deployment

You can refer to the [Django's documentation][deployment-doc] to know how to
deploy FanfareHub.

[deployment-doc]: https://docs.djangoproject.com/en/3.2/howto/deployment/wsgi/

### NGINX

One common way is to use Gunicorn or uWSGI to run the WSGI application and
serve it with NGINX. Here is an example configuration for NGINX which can be
used in the `server` block:

```nginx
location / {
    # with Gunicorn
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header Host $http_host;
    proxy_redirect off;
    proxy_pass http://unix:<wsgi_socket_path>;
    # … or with uWSGI
    include uwsgi_params;
    uwsgi_pass unix:<uwsgi_socket_path>;
}
location /media {
    alias <app_instance_path>/var/media;
}
location /static {
    alias <app_instance_path>/var/static;
    # Optional: don't log access to assets
    access_log off;
}
location = /favicon.ico {
    alias <app_instance_path>/var/static/favicon/favicon.ico;
    # Optional: don't log access to the favicon
    access_log off;
}
```

## Structure
### Overview

All the application files - e.g. Django code including settings, templates and
statics - are located into `fanfarehub/`.

Two environments are defined - either for requirements and settings:
- `development`: for local application development and testing. It uses a
  SQLite3 database and enable debugging by default, add some useful settings
  and applications for development purpose - i.e. the `django-debug-toolbar`.
- `production`: for production. It checks that configuration is set and
  correct, try to optimize performances and enforce some settings - i.e. HTTPS
  related ones.

### Local changes

You can override and extend statics and templates locally. This can be useful
if you have to change the logo for a specific instance for example. For that,
just put your files under the `local/static/` and `local/templates/` folders.

Regarding the statics, do not forget to collect them after that. Note also that
the `local/` folder is ignored by *git*.

### Variable content

All the variable content - e.g. user-uploaded media, collected statics - are
stored inside the `var/` folder. It is also ignored by *git* as it's specific
to each application installation.

So, you will have to configure your Web server to serve the `var/media/` and
`var/static/` folders, which should point to `/media/` and `/static/`,
respectively.

## Development

To set up a development environment, you can follow the [Installation](#installation)
procedure and ensure that `ENV=development` is set in the `config.env` file before
running `make init`. Note that you can still change this variable later and run
`make init` again.

There is some additional rules when developing, which are mainly wrappers for
`manage.py`. You can list all of them by running `make help`. Here are the main ones:
- `make serve`: run a development server
- `make test`: test the whole application
- `make lint`: check the Python code syntax

## License

FanfareHub is developed by La Brigade des Tubes and licensed under the
[AGPLv3+](LICENSE).
