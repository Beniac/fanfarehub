{% load i18n %}
{% autoescape off %}
{% blocktrans with site_name=site.name %}Account activation on {{ site_name }}{% endblocktrans %}
{% endautoescape %}
