import pytest


class ViewMixin:
    """
    Adds facilities for making requests with Webtest to a class which defines
    tests for views.
    """

    #: The default URL to use in requests.
    url = None

    #: The default user which will make requests.
    user = None

    #: Whether CSRF protection must be enabled.
    csrf_checks = True

    @pytest.fixture(autouse=True)
    def setup_django_app(self, django_app_factory):
        self.django_app = django_app_factory(csrf_checks=self.csrf_checks)

    def _query(self, method, url=None, **kwargs):
        """
        Execute the request of type `method` to `url`. For the other available
        arguments, see `webtest.app.TestApp.get`.
        """
        kwargs.setdefault('user', self.user)
        return getattr(self.django_app, method)(url or self.url, **kwargs)

    def get(self, url=None, **kwargs):
        return self._query('get', url, **kwargs)

    def post(self, url=None, **kwargs):
        return self._query('post', url, **kwargs)
